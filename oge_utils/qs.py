# -*- coding: utf-8 -*-
import os
import re
import click
import subprocess

JOB_REGEX = re.compile(r"^Your job(?:-array)? (\d+)\.?")
LOG_DIR = "logs"

DEVNULL = open(os.devnull, "wb")
QSUB = [
    'qsub',  # qsub executable name
    '-h',  # put a user hold on the job by default
    '-o', ':{}/$JOB_ID'.format(LOG_DIR),
    '-e', ':{}/$JOB_ID'.format(LOG_DIR),  # put logs in special directory
]


class QsubOption(click.Option):
    """Do not expose qsub options directly, group them together
    into a dictionary in the context.
    """
    def __init__(self, param_decls=None,
                 **attrs):
        click.Option.__init__(
            self, param_decls,
            expose_value=False,
            callback=lambda ctx, p, v: (ctx.obj.__setitem__(p.opts[0], v)),
            **attrs)


def submit_job(command, options):
    """Submit a job and create the log directory.
    """
    output = subprocess.check_output(QSUB + options + command).decode()
    jobid = JOB_REGEX.match(output).group(1)

    if jobid is not None:
        job_logs = os.path.join(LOG_DIR, jobid)
        os.makedirs(job_logs)

        subprocess.check_call(['qalter', '-h', 'U', jobid],
                              stdout=DEVNULL, stderr=DEVNULL)

    return jobid


def get_tasks(taskfile):
    import json

    data = taskfile.read()
    try:
        tasks = json.loads(data)
    except ValueError:
        tasks = data.split("\n")

    return tasks


@click.command(name='qs', context_settings=dict(
    ignore_unknown_options=True,
    obj=dict(),
))
@click.option('-q', cls=QsubOption)
@click.option('--tasks', type=click.File('r'))
@click.argument("command", nargs=-1, type=click.UNPROCESSED)
@click.pass_context
def main(ctx, tasks, command):
    if not command:
        return

    qsub_options = []
    for opt, val in filter(lambda x: x[1] is not None, ctx.obj.items()):
        qsub_options += [opt, val]

    command = list(command)

    taskfile = tasks
    if taskfile is not None:
        tasks = get_tasks(taskfile)
        ntasks = len(tasks)
        qsub_options += ['-t', '1:{}'.format(ntasks)]
        command.append(os.path.abspath(taskfile.name))

    jobid = submit_job(command, qsub_options)
    if jobid is not None:
        click.secho("Your job {} was submitted\n".format(jobid), fg='green')
    else:
        click.secho("Job submission failed\n", fg='red')


if __name__ == '__main__':
    main()
