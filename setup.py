"""
oge_utils
---------

OGE Utils is a set of scripts to help you wrangle with Open Grid Engine.
"""
import re
import ast
from setuptools import setup


_version_re = re.compile(r'__version__\s+=\s+(.*)')

with open('oge_utils/__init__.py', 'rb') as f:
    version = str(ast.literal_eval(_version_re.search(
        f.read().decode('utf-8')).group(1)))


setup(
    name='oge_utils',
    version=version,
    url="https://bitbucket.org/sixpi/oge-utils",
    license='MIT',
    author="Bing Xia",
    author_email="sixpi@bu.edu",
    description="Open Grid Engine utilities",
    long_description=__doc__,
    packages=['oge_utils'],
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=[
        'click>=5.0',
        'beautifulsoup4>=4.4',
        'lxml>=3.4'
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Scientific/Engineering',
        'Topic :: Utilities',
    ],

    keywords='OGE',
    use_2to3=True,

    entry_points={
        'console_scripts': [
            "qs = oge_utils.qs:main",
            "qblame = oge_utils.qblame:main"
        ]
    }
)
